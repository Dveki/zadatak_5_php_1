<!DOCTYPE html>
<html lang="en">
<head>
    <title>Domaci_1</title>
</head>
<body>
<div>
<h2>Zadatak 1</h2>
    <p> Napraviti web stranicu koja sadrži definicije promeljnivih tipa: int, string, float, bool i array. Vrednosti promenljivih ispisati različitim bojama.</p>
<?php
$niztipova = array ('Jovan', 678, 0.35, true, false, [56, 17, 'Marko']);
var_dump($niztipova);
    for ($x = 0; $x < count($niztipova); $x++) {
        if (is_int($niztipova[$x])) {
            echo ' Integer – podržan dekadni, oktalni i heksadekadni zapis broja, u zadatom nizu na poziciji ' . $x. ' nalazi se integer: ' . '<span style="color:blue;">' . $niztipova[$x] . '</span><br>';
        }
        elseif (is_float($niztipova[$x])) {
            echo 'Float – broj sa decimalnom tackom, u zadatom nizu na poziciji ' . $x . ' nalazi se float: ' . '<span style="color:green;">' . $niztipova[$x] . '</span><br>';
        }
        elseif (is_string($niztipova[$x])) {
            echo 'String – niske, mogu se koristiti jednostuki ili dvostruki navodnici, u zadatom nizu na poziciji  ' . $x . ' nalazi se string: ' . '<span style="color:brown;">' . $niztipova[$x] . '</span><br>';
        }
        elseif (is_bool($niztipova[$x])) {
            if ($niztipova[$x] == true) {
                $niztipova[$x] = 'true';
            }
            else {
                $niztipova[$x] = 'false';
            }
            echo 'Boolean – logicki tip: vrednosti true i false, u zadatom nizu na poziciji  ' . $x . ' nalazi se boolean: ' . '<span style="color:purple;">' . $niztipova[$x] . '</span><br>';                       
            }
        elseif (is_array($niztipova[$x])) {
            echo 'Array – nizovi, zadaju se sa array, u zadatom nizu na poziciji  ' . $x . ' nalazi se array sa elementima: ' .'<span style="color:brown;">' . $niztipova[$x][0].','.$niztipova[$x][1].','.$niztipova[$x][2] . '</span><br>';                       
            }
        } 
    ?>
    </div>
    <div>
    <h2>Zadatak 2</h2>
    <p> Definisati funkciju koja prima dva parametra, prvi je tipa string i sadrži engleski naziv boje (red, blue ili green) a drugi parametar je niz sa brojevima. Funkcija treba da izračuna sumu tih brojeva i da je ispiše u boji koja je prosleđena preko prvog parametra.</p>
    <?php 
    include 'functions.php';
    $novi_broj = array (1,2,36,4,5,23,7,8,9,10,23,45);
    $nova_boja = "red"; 
    sumbr($novi_broj, $nova_boja);
    $novi_broj = array (1,56,36,4,5,23,7,56,9,100,23,45);
    $nova_boja = "blue";
    sumbr($novi_broj, $nova_boja);
    $novi_broj = array (1,2,36,4,44,23,78,8,9,10,23,45);
    $nova_boja = "green";
    sumbr($novi_broj, $nova_boja);
    ?>
    </div>
</body>
</html>